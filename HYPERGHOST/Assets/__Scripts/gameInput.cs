﻿using UnityEngine;
using System.Collections;

public class gameInput : MonoBehaviour {
	public enum axisType{horizontal, vertical, rightHor, rightVer};
	public enum buttonType{a,b,x,y};
	
	public static int keyboardTestingInt = -1;
	
	public static float GetAxis(gameInput.axisType type, int player = 1){
		if (player == gameInput.keyboardTestingInt) { // keyboard input
			if (type == axisType.horizontal) return Input.GetAxis ("Horizontal");
			else return Input.GetAxis("Vertical");
		}
		
		switch (type){
		case(axisType.horizontal):
			return Input.GetAxis("L_XAxis_"+player);
		case(axisType.vertical):
			return -Input.GetAxis("L_YAxis_"+player);
		case(axisType.rightHor):
			return Input.GetAxis("R_XAxis_"+player);
		case(axisType.rightVer):
			return -Input.GetAxis("R_YAxis_"+player);
		default:
			return 0;
		}
	}
	/*public static bool GetButtonDown(ControllerStuff.Button type, int player = 1){
		if (player == keyboardTestingInt) {
			if (type == buttonType.a) return Input.GetKeyDown (KeyCode.LeftShift); // charge
			else if (type == buttonType.x) return Input.GetKeyDown(KeyCode.Space); // sound ping
			else return false;
		}
		
		switch (type) {
		case(buttonType.a):
			return Input.GetButtonDown("A_"+player);
		case(buttonType.b):
			return Input.GetButtonDown("B_"+player);
		case(buttonType.x):
			return Input.GetButtonDown("X_"+player);
		case(buttonType.y):
			return Input.GetButtonDown("Y_"+player);
		default:
			return false;
		}
	}*/
	public static bool GetButton(gameInput.buttonType type, int player = 1){
		if (player == keyboardTestingInt) {
			if (type == buttonType.a) return Input.GetKey (KeyCode.LeftShift); // charge
			else if (type == buttonType.x) return Input.GetKey(KeyCode.Space); // sound ping
			else return false;
		}
		
		switch (type) {
		case(buttonType.a):
			return Input.GetButton("A_"+player);
		case(buttonType.b):
			return Input.GetButton("B_"+player);
		case(buttonType.x):
			return Input.GetButton("X_"+player);
		case(buttonType.y):
			return Input.GetButton("Y_"+player);
		default:
			return false;
		}
	}
	
	public static bool GetButtonUp(gameInput.buttonType type, int player = 1){
		if (player == keyboardTestingInt) {
			if (type == buttonType.a) return Input.GetKeyUp (KeyCode.LeftShift); // charge
			else if (type == buttonType.x) return Input.GetKeyUp (KeyCode.Space); // sound ping
			else return false;
		}
		
		switch (type) {
		case(buttonType.a):
			return Input.GetButtonUp("A_"+player);
		case(buttonType.b):
			return Input.GetButtonUp("B_"+player);
		case(buttonType.x):
			return Input.GetButtonUp("X_"+player);
		case(buttonType.y):
			return Input.GetButtonUp("Y_"+player);
		default:
			return false;
		}
	}
	
	
}
