﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class FrogMovement : MonoBehaviour {
	public bool isTeacherFrog;
	public int playerNum;

	private Rigidbody2D frogRigidbody2D;
	private bool bMoving = false;
	private Vector2 moveVector = Vector2.zero;
	private float moveDistance = 2.0f;
	private bool bMovingRight = false;
	private Vector2 moveStart = Vector2.zero;
	private Animator animator;

	/**TEST TEST TEST for vibration
	 * 
	 * 
	 */
	//ControllerStuff vib = new ControllerStuff();

	void Start () {
		if (isTeacherFrog) playerNum = 0;
		frogRigidbody2D = GetComponent<Rigidbody2D>();
	    animator = GetComponent<Animator>();
	}

	void Update () {
		bMoving = frogRigidbody2D.velocity != Vector2.zero;

		ControllerStuff.Button downButton;
		ControllerStuff.Button upButton;
		ControllerStuff.Button leftButton;
		ControllerStuff.Button rightButton;

		if (playerNum == 0 || GameManager.s.debugMode) {
			downButton = ControllerStuff.Button.DPadDown;
			upButton = ControllerStuff.Button.DPadUp;
			leftButton = ControllerStuff.Button.DPadLeft;
			rightButton = ControllerStuff.Button.DPadRight;
		} else {
			downButton = LilyManager.s.frog_moveDown;
			upButton = LilyManager.s.frog_moveUp;
			leftButton = LilyManager.s.frog_moveLeft;
			rightButton = LilyManager.s.frog_moveRight;
		}

		if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, upButton) || Input.GetKeyDown (KeyCode.I)) {
			PressedUp();
		}

		else if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, rightButton) || Input.GetKeyDown (KeyCode.L)) {
			PressedRight();
		}
		else if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, downButton) || Input.GetKeyDown (KeyCode.K)) {
			PressedDown();
		}
		else if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, leftButton) || Input.GetKeyDown (KeyCode.J)) {
			PressedLeft();
		}
	}

	void FixedUpdate () {
		if(moveVector != Vector2.zero) {
			animator.SetBool("bMoving", moveVector != Vector2.zero);
			if (frogRigidbody2D.position == moveStart + moveVector)
			{
				StopMoving();
			}
			float fractionOfJourney = (frogRigidbody2D.position - (frogRigidbody2D.position + moveVector)).magnitude / (moveStart + moveVector - moveStart).magnitude;
			Vector2 newPosition = Vector2.Lerp(frogRigidbody2D.position, frogRigidbody2D.position + moveVector, 1/10.0f);
			frogRigidbody2D.position = newPosition;
		}
	}

	void StopMoving() {
		moveVector = Vector2.zero;
		animator.SetBool("bMoving", moveVector != Vector2.zero);
	}
	
	void PressedUp(){
		
	Debug.Log("PressedUp");
		if (moveVector == Vector2.zero || !isTeacherFrog)
		{
			MoveUp();
		}
	}

	void PressedDown()
	{
		Debug.Log("PressedDown");
		if (moveVector == Vector2.zero || !isTeacherFrog)
		{
			MoveDown();
		}
	}

	void PressedLeft()
	{
		Debug.Log("PressedLeft");
		if (moveVector == Vector2.zero || !isTeacherFrog)
		{
			MoveLeft();
		}
	}

	void PressedRight()
	{
		Debug.Log("PressedRight");
		if (moveVector == Vector2.zero || !isTeacherFrog)
		{
			MoveRight();
		}
	}

	void MoveLeft()
	{
		Debug.Log ("moving left");
		moveVector = Vector2.left * moveDistance;
		moveStart = frogRigidbody2D.position;
		transform.rotation = Quaternion.Euler (0f, 0f, 90f);
	}

	void MoveUp()
	{
		Debug.Log ("moving up");
		moveVector = Vector2.up * moveDistance;
		moveStart = frogRigidbody2D.position;
		transform.rotation = Quaternion.Euler (0f, 0f, 0f);
	}

	void MoveDown()
	{
		Debug.Log ("moving down");
		moveVector = Vector2.down * moveDistance;
		moveStart = frogRigidbody2D.position;
		transform.rotation = Quaternion.Euler (0f, 0f, 180f);
	}

	void MoveRight()
	{
		Debug.Log ("moving right");
		moveVector = Vector2.right * moveDistance;
		moveStart = frogRigidbody2D.position;
		transform.rotation = Quaternion.Euler (0f, 0f, -90f);
	}

	void OnCollisionEnter2D(Collision2D collision2D) {
//		moveStart = moveStart + moveVector;
//		moveVector *= -1f;
		if (collision2D.collider.CompareTag("Wall")) {
			Debug.Log ("FROGGIE HIT ME");
			StopMoving();
			transform.position = moveStart;
		}
	}
}
	






