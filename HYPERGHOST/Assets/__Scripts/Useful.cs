﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Useful : MonoBehaviour {
	public delegate void PassNothing ();

	public static List<T> ShuffleList<T> (List<T> list) {
		List<T> shuffledList = new List<T> ();
		List<int> indices = new List<int> ();
		for (int i=0; i<list.Count; i++) {
			indices.Add (i);
		}

		while (indices.Count > 0) {
			int index = indices [Random.Range (0, indices.Count-1)];
			shuffledList.Add (list [index]);
			indices.Remove (index);
		}

		return shuffledList;
	}
}
