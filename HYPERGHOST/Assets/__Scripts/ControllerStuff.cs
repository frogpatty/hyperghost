﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class ControllerStuff : MonoBehaviour {

	private static ControllerStuff m_myPrivateSelf;
	public static ControllerStuff s {
		get {
			if (m_myPrivateSelf == null) m_myPrivateSelf = Object.FindObjectOfType<ControllerStuff>();
			return m_myPrivateSelf;
		}
	}
	
	public float vib = 0.9f;
	public float calm = 0.0f;

	public enum Button {
		A,
		B,
		X,
		Y,
		LeftBumper,
		RightBumper,
		Select,
		Start,
		LeftTrigger,
		RightTrigger,
		DPadDown,
		DPadLeft,
		DPadUp,
		DPadRight
	}
	
	public enum Trigger {
		Left,
		Right
	}

	public bool debugMessageOnPressA;
	
	public static bool[] playerIndexSet = new bool[4];
	bool[] playerIndexTaken = new bool[4];
	public PlayerIndex[] playerIndex = new PlayerIndex[4];
	GamePadState[] state = new GamePadState[4];
	GamePadState[] prevState = new GamePadState[4];
	
	void Awake() {
		connectGamepads();
	}
	
	void Update() {
		connectGamepads ();
	}
	
	void connectGamepads () {
		for (int i=0; i<4; i++) {
			if (!playerIndexSet[i] || !state[i].IsConnected) {
				for (int j=0; j<4; j++) { // ++j is because PlayerIndex is goes One Two Three Four rather than 0 1 2 3
					if (!playerIndexTaken[j]) {
						PlayerIndex testPlayerIndex = (PlayerIndex)j;
						if (GamePad.GetState (testPlayerIndex).IsConnected) {
							playerIndex[i] = testPlayerIndex;
							Debug.Log (string.Format ("Player {0} is on Controller {1}.",i,playerIndex[i]));
							playerIndexSet[i] = true;
							playerIndexTaken[j] = true;
							break;
						}
					}
				}
			}
			prevState[i] = state[i];
			state[i] = GamePad.GetState(playerIndex[i]);
		}
		if (debugMessageOnPressA) checkTheFolks();
	}
	
	public bool getButtonUp(PlayerIndex player, Button button) {
		ButtonState currButtonState = ButtonState.Released;
		ButtonState prevButtonState = ButtonState.Released;
		
		getButtonStates (player, button, ref currButtonState, ref prevButtonState);
		
		if (currButtonState == ButtonState.Released && prevButtonState == ButtonState.Pressed) {
			Debug.Log (player + " just released " + button.ToString());
			return true;
		} else {
			//print ("curr state is " + currButtonState.ToString() + "// prev state is " + prevButtonState.ToString ());
			return false;
		}
	}
	
	public bool getButtonDown(PlayerIndex player, Button button) {
		ButtonState currButtonState = ButtonState.Released;
		ButtonState prevButtonState = ButtonState.Released;
		
		getButtonStates (player, button, ref currButtonState, ref prevButtonState);
		
		if (currButtonState == ButtonState.Pressed && prevButtonState == ButtonState.Released) {
			Debug.Log (player + " just pressed " + button.ToString());
			return true;
		} else {
			//print ("curr state is " + currButtonState.ToString() + "// prev state is " + prevButtonState.ToString ());
			return false;
		}
	}
	
	public bool getTriggerDown(PlayerIndex player, Trigger trigger, float _triggerThreshold) {
		bool currTriggerDown = false;
		bool prevTriggerDown = false;
		
		getTriggerStates (player, trigger, ref currTriggerDown, ref prevTriggerDown, _triggerThreshold);
		
		if (currTriggerDown && !prevTriggerDown) {
			Debug.Log (player + " just pressed the " + trigger.ToString() + " trigger.");
			return true;
		} else {
			return false;
		}
	}
	
	public bool getTriggerUp(PlayerIndex player, Trigger trigger, float _triggerThreshold) {
		bool currTriggerDown = false;
		bool prevTriggerDown = false;
		
		getTriggerStates (player, trigger, ref currTriggerDown, ref prevTriggerDown, _triggerThreshold);
		
		if (!currTriggerDown && prevTriggerDown) {
			Debug.Log (player + " just released the " + trigger.ToString() + " trigger.");
			return true;
		} else {
			return false;
		}
	}
	
	public bool getTrigger(PlayerIndex player, Trigger trigger, float _triggerThreshold) {
		int index = (int)player;
		if (trigger == Trigger.Left) {
			return state[index].Triggers.Left >= _triggerThreshold;
		} else if (trigger == Trigger.Right) {
			return state[index].Triggers.Right >= _triggerThreshold;
		} else {
			Debug.Log ("holy fugg what");
			return false;
		}
	}
	
	void getTriggerStates(PlayerIndex player, Trigger trigger, ref bool currTriggerDown, ref bool prevTriggerDown, float _triggerThreshold) {
		int index = (int)player;
		
		switch(trigger) {
		case Trigger.Left:
			currTriggerDown = state [index].Triggers.Left >= _triggerThreshold;
			prevTriggerDown = prevState [index].Triggers.Left >= _triggerThreshold;
			break;
		case Trigger.Right:
			currTriggerDown = state [index].Triggers.Right >= _triggerThreshold;
			prevTriggerDown = prevState [index].Triggers.Right >= _triggerThreshold;
			break;
		}
	}
	
	void getButtonStates(PlayerIndex player, Button button, ref ButtonState currButtonState, ref ButtonState prevButtonState) {
		int index = (int)player;
		
		switch (button) {
		case Button.A:
			currButtonState = state[index].Buttons.A;
			prevButtonState = prevState[index].Buttons.A;
			break;
		case Button.B:
			currButtonState = state[index].Buttons.B;
			prevButtonState = prevState[index].Buttons.B;
			break;
		case Button.X:
			currButtonState = state[index].Buttons.X;
			prevButtonState = prevState[index].Buttons.X;
			break;
		case Button.Y:
			currButtonState = state[index].Buttons.Y;
			prevButtonState = prevState[index].Buttons.Y;
			break;
		case Button.LeftBumper:
			currButtonState = state[index].Buttons.LeftShoulder;
			prevButtonState = prevState[index].Buttons.LeftShoulder;
			break;
		case Button.RightBumper:
			currButtonState = state[index].Buttons.RightShoulder;
			prevButtonState = prevState[index].Buttons.RightShoulder;
			break;
		case Button.LeftTrigger:
			currButtonState = getButtonStateFromTriggerPressure(state[index].Triggers.Left);
			prevButtonState = getButtonStateFromTriggerPressure(prevState[index].Triggers.Left);
			break;
		case Button.RightTrigger:
			currButtonState = getButtonStateFromTriggerPressure(state[index].Triggers.Right);
			prevButtonState = getButtonStateFromTriggerPressure(prevState[index].Triggers.Right);
			break;
		case Button.Select:
			currButtonState = state[index].Buttons.Back;
			prevButtonState = prevState[index].Buttons.Back;
			break;
		case Button.Start:
			currButtonState = state[index].Buttons.Start;
			prevButtonState = prevState[index].Buttons.Start;
			break;
		case Button.DPadDown:
			currButtonState = state[index].DPad.Down;
			prevButtonState = prevState[index].DPad.Down;
			break;
		case Button.DPadRight:
			currButtonState = state[index].DPad.Right;
			prevButtonState = prevState[index].DPad.Right;
			break;
		case Button.DPadUp:
			currButtonState = state[index].DPad.Up;
			prevButtonState = prevState[index].DPad.Up;
			break;
		case Button.DPadLeft:
			currButtonState = state[index].DPad.Left;
			prevButtonState = prevState[index].DPad.Left;
			break;
		}
	}

	ButtonState getButtonStateFromTriggerPressure (float pressure) {
		if (pressure >= 0.5f) return ButtonState.Pressed;
		else return ButtonState.Released;
	}
	
	void checkTheFolks() {
		for (int i=0; i<4; i++) {
			if (playerIndexSet[i]) {
				if (state[i].Buttons.A == ButtonState.Pressed) {
					print ("Player " + i + " on gamepad " + playerIndex[i]);
				}
			}
		}
	}

	public void vibrationBurst(PlayerIndex pIndex, float lMotor, float rMotor, float duration) {
		if (!currVibratingGamepads [(int)pIndex]) {
			StartCoroutine (doVibrationBurst (pIndex, lMotor, rMotor, duration));
		} else {
			Debug.LogErrorFormat("can't vibrate controller {0} because it is already vibrating!", pIndex.ToString());
		}
	}

	bool[] currVibratingGamepads = new bool[4];

	IEnumerator doVibrationBurst(PlayerIndex pIndex, float lMotor, float rMotor, float duration) {
		currVibratingGamepads [(int)pIndex] = true;
		float timer = 0.0f;
		while (timer < duration) {
			GamePad.SetVibration (pIndex, lMotor, rMotor);
			timer += Time.deltaTime;
			yield return null;
		}
		GamePad.SetVibration (pIndex, 0, 0);
		currVibratingGamepads [(int)pIndex] = false;
	}

	public void vibrationCollision(int call){

		/**
		 * if the lillypad and the tadpole have the same value 
		 * then there is aninstand vibration
		 * 
		 */
		//if(lillypadButtonValue==tadpollbuttion valus)


		/*if (call > 1) {
			GamePad.SetVibration (0, vib, vib);
			print ("good Vibes");
		} else {
			GamePad.SetVibration (0, calm, calm);
			print ("no Vibes");
		}*/


	}

	void OnLevelWasLoaded(int level) {
		foreach (PlayerIndex p in System.Enum.GetValues(typeof(PlayerIndex))) {
			GamePad.SetVibration(p,0,0);
		}
	}

	void OnApplicationQuit() {
		foreach (PlayerIndex p in System.Enum.GetValues(typeof(PlayerIndex))) {
			GamePad.SetVibration(p,0,0);
		}
	}





}
