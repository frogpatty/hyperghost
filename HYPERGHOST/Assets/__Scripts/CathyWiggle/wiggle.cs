﻿using UnityEngine;
using System.Collections;

public class wiggle : MonoBehaviour {
	Quaternion originalRot;
	void Start () {
		originalRot = transform.localRotation;
	}

	public void Wiggle(){
		StartCoroutine(theWiggle());
		AudioSource a = GetComponent<AudioSource> ();
		if (a != null) a.Play();
	}

	IEnumerator theWiggle()
	{
		float wigAngle= 18f;
		float delayTime = 0.18f;

		gameObject.transform.Rotate (0f, 0f, wigAngle);
		yield return new WaitForSeconds (delayTime);
		gameObject.transform.Rotate (0f, 0f, -wigAngle*2);
		yield return new WaitForSeconds (delayTime);
		gameObject.transform.Rotate (0f, 0f, wigAngle);
		yield return new WaitForSeconds (delayTime);
		gameObject.transform.Rotate (0f, 0f, wigAngle);
		yield return new WaitForSeconds (delayTime);
		gameObject.transform.Rotate (0f, 0f, -wigAngle*2);
		yield return new WaitForSeconds (delayTime);
		//gameObject.transform.Rotate (0f, 0f, wigAngle);
		gameObject.transform.localRotation = originalRot;
	}
}
