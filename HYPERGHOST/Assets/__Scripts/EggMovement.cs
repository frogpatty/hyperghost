﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;

[RequireComponent(typeof(wiggle))]
public class EggMovement : MonoBehaviour {
	public int playerNum;

	public List<Sprite> eggSprites;
	public Sprite finalSprite;
	public SpriteRenderer spriteRenderer;
	public int currTier;
	public int currWiggle;
	public int wigglesPerTier;
	public int numTiers;

	private wiggle myWiggle;
	private bool canWiggle = false;
	private LilyManager lilyManager;

	private bool m_isTadpole = false;
	public bool isTadpole {
		get { return m_isTadpole; }
	}

	public Useful.PassNothing OnBecomeTadpole;

	void Start () {
		lilyManager = FindObjectOfType<LilyManager>();
		if (playerNum == 0) playerNum = 1;
		numTiers = eggSprites.Count;
		currTier = 0; currWiggle = 0;
		spriteRenderer.sprite = eggSprites [0];
		myWiggle = GetComponent<wiggle> ();
		enableWiggling ();
	}

	public void enableWiggling() {
		canWiggle = true;
	}

	void Update () {
		ControllerStuff.Button wiggleButton;

		if (GameManager.s.debugMode) {
			wiggleButton = ControllerStuff.Button.A;
		} else {
			wiggleButton = LilyManager.s.egg_hatchButton;
		}

		if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, wiggleButton) || Input.GetKeyDown (KeyCode.Space)) {
			wigglyWiggle();
		} else {
			foreach (ControllerStuff.Button badButton in lilyManager.currbadButtons) {
				if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, badButton)) {
					lilyManager.badButtonPressed(playerNum);
				}
			}
		}
	}

	void wigglyWiggle() {
		if (canWiggle) {
			myWiggle.Wiggle ();
			currWiggle++;
			if (currWiggle >= wigglesPerTier) {
				currTier++;
				if (currTier >= numTiers) {
					becomeTadpole ();
				} else {
					currWiggle = 0;
					evolve();
				}
			}
		}
	}

	void evolve() {
		//[] play a sound??
		spriteRenderer.sprite = eggSprites [currTier];
	}

	void becomeTadpole() {
		canWiggle = false;
		m_isTadpole = true;
		spriteRenderer.sprite = finalSprite;
		Debug.Log ("i am now a tadpole!");
		if (OnBecomeTadpole != null) OnBecomeTadpole();
	}
}
