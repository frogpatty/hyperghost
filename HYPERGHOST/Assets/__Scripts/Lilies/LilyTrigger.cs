﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class LilyTrigger : MonoBehaviour {
	private Lily myLily;

	public void setLily (Lily l) {
		myLily = l;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Frog") {
			Debug.LogFormat("frog just hit lily {0}!!!", myLily.myButton.ToString());
		}
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.tag == "Frog") {
			if (LilyManager.s.currMvmtButtons.Contains (myLily.myButton)) {
				GamePad.SetVibration(PlayerIndex.One, 0.8f, 0.8f);
			}
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Frog") {
			GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
		}
	}
}
