﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LilyManager : MonoBehaviour {
	private static LilyManager m_myPrivateSelf;
	public int badButtonsPressedThisPhase = 0;
	static int MAX_BAD_BUTTON_PRESSES_PER_PHASE = 3;
	public static LilyManager s {
		get {
			if (m_myPrivateSelf == null) m_myPrivateSelf = Object.FindObjectOfType<LilyManager>();
			return m_myPrivateSelf;
		}
	}

	public GameObject prf_lilyPad;
	public List<ControllerStuff.Button> currMvmtButtons; // list of buttons that currently result in some kind of movement
	public List<ControllerStuff.Button> currbadButtons; // list of buttons that currently result in some kind of movement
	public List<Transform> lilySpawnPoints; // the transforms where lilies will spawn
	public List<Lily> currLilies; // list of currently spawned lilies
	public AudioClip pleaseNoAudioClip;

	private List<ControllerStuff.Button> candidateButtons = new List<ControllerStuff.Button>(); // list of buttons that we're allowed to pull from -- ex.

	public ControllerStuff.Button egg_hatchButton;
	public ControllerStuff.Button tadpole_moveUp;
	public ControllerStuff.Button tadpole_moveDown;
	public ControllerStuff.Button frog_moveUp;
	public ControllerStuff.Button frog_moveLeft;
	public ControllerStuff.Button frog_moveDown;
	public ControllerStuff.Button frog_moveRight;
	private RedX[] redXs;
	private AudioSource audioSource;

	void Start() {
		currLilies = new List<Lily> ();
		initializeCandidateButtons ();
		audioSource = GetComponent<AudioSource>();
//		redXs = new List<RedX>(FindObjectsOfType<RedX>());
		redXs = FindObjectsOfType<RedX>();
		foreach (RedX redX in redXs) {
			redX.gameObject.SetActive(false);
		}
	}

	void initializeCandidateButtons() {
		foreach (ControllerStuff.Button b in System.Enum.GetValues(typeof(ControllerStuff.Button))) {
			candidateButtons.Add (b);
		}
		candidateButtons.Remove (ControllerStuff.Button.DPadDown);
		candidateButtons.Remove (ControllerStuff.Button.DPadUp);
		candidateButtons.Remove (ControllerStuff.Button.DPadLeft);
		candidateButtons.Remove (ControllerStuff.Button.DPadRight);
	}

	void initializeBadButtons() {
		currbadButtons = new List<ControllerStuff.Button>();
		foreach (ControllerStuff.Button b in System.Enum.GetValues(typeof(ControllerStuff.Button))) {
			currbadButtons.Add (b);
		}
		currbadButtons.Remove (ControllerStuff.Button.DPadDown);
		currbadButtons.Remove (ControllerStuff.Button.DPadUp);
		currbadButtons.Remove (ControllerStuff.Button.DPadLeft);
		currbadButtons.Remove (ControllerStuff.Button.DPadRight);
	}
	
	void Update() {
		/*if (Input.GetKeyDown (KeyCode.J))
			spawnLilies (GameManager.Phase.Egg);
		else if (Input.GetKeyDown (KeyCode.K))
			spawnLilies (GameManager.Phase.Tadpole);
		else if (Input.GetKeyDown (KeyCode.L))
			spawnLilies (GameManager.Phase.Frog);*/
	}

	public bool transitioning = false;

	public void spawnLilies(GameManager.Phase phase) {
		StartCoroutine ("doSpawnLilies", phase);
	}

	IEnumerator doSpawnLilies(GameManager.Phase phase) {
		if (transitioning) yield break;

		transitioning = true;
		float timeEstimate = destroyLilies ();
		//yield return new WaitForSeconds (timeEstimate);
		while (destroyingLilies) {
			yield return null;
		}
		transitioning = true;

		//[x] calculate which buttons are special
		randomizeMvmtButtons (phase);

		//[x] mix our good buttons in with our dud buttons
		int numLiliesToSpawn = getNumLiliesFromPhase (phase);

		List<ControllerStuff.Button> shuffledDuds = new List<ControllerStuff.Button> (candidateButtons);
		shuffledDuds = Useful.ShuffleList<ControllerStuff.Button> (shuffledDuds);
		List<ControllerStuff.Button> liliesToSpawn = new List<ControllerStuff.Button> (currMvmtButtons);
		int counter = 0;
		while (liliesToSpawn.Count < numLiliesToSpawn) {
			if (counter >= shuffledDuds.Count) {
				Debug.LogError ("YIKES!!! YOU'RE OUT OF DUDS D: !!!");
				counter = 0;
			}
			if (!liliesToSpawn.Contains(shuffledDuds[counter])) {
				liliesToSpawn.Add (shuffledDuds[counter]);
			}
			counter++;
		}
		liliesToSpawn = Useful.ShuffleList<ControllerStuff.Button> (liliesToSpawn);

		//[x] iterate through those and instantiate
		StartCoroutine ("delayed_spawnLilies", liliesToSpawn);

	}

	IEnumerator delayed_spawnLilies(List<ControllerStuff.Button> liliesToSpawn) {
		for (int i=0; i<liliesToSpawn.Count; i++) {
			transitioning = true; // sloppy af
			if (i >= lilySpawnPoints.Count) {
				Debug.LogError ("YOU DON'T HAVE ENOUGH SPAWN POINTS!!! D:");
				yield break;
			}
			GameObject go = Instantiate(prf_lilyPad, lilySpawnPoints[i].position, lilySpawnPoints[i].rotation) as GameObject;
			Lily lily = go.GetComponent<Lily>();
			lily.initializeLily(liliesToSpawn[i]);
			currLilies.Add (lily);
			yield return new WaitForSeconds(0.05f);
		}
		transitioning = false;
	}

	bool destroyingLilies = false;

	public float destroyLilies() {
		transitioning = true;
		destroyingLilies = true;
		float delay = 0.05f;
		float timeEstimate = (delay) * currLilies.Count;
		timeEstimate += Lily.lilyPopTime;
		StartCoroutine ("delayed_destroyLilies", delay);
		return timeEstimate;
	}

	IEnumerator delayed_destroyLilies(float delay) {
		for (int i=0; i<currLilies.Count; i++) {
			Lily l = currLilies[i];
			l.popOut();
			yield return new WaitForSeconds(delay);
		}
		currLilies.Clear ();
		transitioning = false;
		destroyingLilies = false;
	}

	public void badButtonPressed(int playerNum) {
		badButtonsPressedThisPhase++;
		ControllerStuff.s.vibrationBurst ((XInputDotNetPure.PlayerIndex)playerNum, 0.8f, 0.8f, 0.5f);
		audioSource.PlayOneShot(pleaseNoAudioClip);

		Debug.Log("size" + redXs.Length);
		if (badButtonsPressedThisPhase == 1) {
			foreach (RedX redX in redXs) {
				if (redX.gameObject.name == "redX1") {
					redX.gameObject.SetActive(true);
				}
			}
		}
		if (badButtonsPressedThisPhase == 2) {
			foreach (RedX redX in redXs) {
				if (redX.gameObject.name == "redX2") {
					redX.gameObject.SetActive(true);
				}
			}
		}
		if (badButtonsPressedThisPhase == 3) {
			foreach (RedX redX in redXs) {
				if (redX.gameObject.name == "redX3") {
					redX.gameObject.SetActive(true);
				}
			}
		}
		
		if (badButtonsPressedThisPhase >= MAX_BAD_BUTTON_PRESSES_PER_PHASE) {
			lose();
		}
	}

	public void lose() {
		GameManager gameManager = FindObjectOfType<GameManager>();
		gameManager.Reset();
	}

	public void randomizeMvmtButtons(GameManager.Phase phase) {
		currMvmtButtons.Clear ();
		int numButtons = getNumButtonsFromPhase (phase);
		List<ControllerStuff.Button> temp = new List<ControllerStuff.Button> (candidateButtons);
		int counter = 0;
		initializeBadButtons();
		while (currMvmtButtons.Count < numButtons) {
			int index = Random.Range (0,temp.Count);
			ControllerStuff.Button buttonToAdd = temp[index];
			currMvmtButtons.Add (buttonToAdd);
			currbadButtons.Remove(buttonToAdd);
			temp.RemoveAt(index);
			setButtonForPhase(counter, buttonToAdd, phase);
			counter++;

//			Debug.LogFormat("Phase {0}: Added Button {1} to list.", phase.ToString(), buttonToAdd);
		}
	}

	// buttonNum is the n in "this is the nth button we're adding to the list
	void setButtonForPhase(int buttonNum, ControllerStuff.Button button, GameManager.Phase phase) {
		if (phase == GameManager.Phase.Egg) {
			if (buttonNum == 0) {
				Debug.LogFormat("wiggle button is: {0}", button);
				egg_hatchButton = button;
			}
		}

		if (phase == GameManager.Phase.Tadpole) {
			if (buttonNum == 1) {
				Debug.LogFormat("up button is: {0}", button);
				tadpole_moveUp = button;
			}
			if (buttonNum == 0) {
				tadpole_moveDown = button;
				Debug.LogFormat("down button is: {0}", button);
			}
		}

		if (phase == GameManager.Phase.Frog) {
			if (buttonNum == 0) frog_moveUp = button;
			if (buttonNum == 1) frog_moveLeft = button;
			if (buttonNum == 2) frog_moveDown = button;
			if (buttonNum == 3) frog_moveRight = button;
		}
	}

	// returns number of buttons we'll be using in a given phase
	public int getNumButtonsFromPhase(GameManager.Phase phase) {
		switch (phase) {
		case GameManager.Phase.Egg:
			return 1;
		case GameManager.Phase.Tadpole:
			return 2;
		case GameManager.Phase.Frog:
			return 4;
		default:
			return 0;
		}
	}

	// returns number of lilies to spawn in a given phase
	public int getNumLiliesFromPhase(GameManager.Phase phase) {
		switch (phase) {
		case GameManager.Phase.Egg:
			return 4;
		case GameManager.Phase.Tadpole:
			return 6;
		case GameManager.Phase.Frog:
			return 10;
		default:
			return 0;
		}
	}

}
