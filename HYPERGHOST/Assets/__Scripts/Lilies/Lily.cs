﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lily : MonoBehaviour {
	GameObject lilyObj;
	public List<Sprite> possibleLilies;
	public SpriteRenderer spriteRenderer;
	public ControllerStuff.Button myButton;
	public TextMesh myLabel;

	public static float lilyPopTime = 0.4f;

	void Awake() {
		GetComponentInChildren<LilyTrigger> ().setLily (this);
		Vector2 rot = new Vector3 (0, 0, Random.Range (0f, 360f));
		transform.eulerAngles = rot;
		spriteRenderer.sprite = possibleLilies [Random.Range (0, possibleLilies.Count)];
	}

	void Start() {
		Vector2 rot = new Vector3 (0, 0, Random.Range (0f, 360f));
		transform.eulerAngles = rot;
	}

	public void initializeLily(ControllerStuff.Button b) {
		myButton = b;
		myLabel.text = getControlText (b);
		popIn ();
	}

	public void popIn() {
		iTween.ScaleFrom (gameObject, Vector3.zero, lilyPopTime);
	}

	public void popOut() {
		iTween.ScaleTo (gameObject, Vector3.zero, lilyPopTime);
		Destroy (gameObject, lilyPopTime);
	}

	public static string getControlText(ControllerStuff.Button button) {
		switch (button) {
		case ControllerStuff.Button.A:
			return "A";
		case ControllerStuff.Button.B:
			return "B";
		case ControllerStuff.Button.LeftBumper:
			return "LB";
		case ControllerStuff.Button.LeftTrigger:
			return "LT";
		case ControllerStuff.Button.RightBumper:
			return "RB";
		case ControllerStuff.Button.RightTrigger:
			return "RT";
		case ControllerStuff.Button.Select:
			return "BACK";
		case ControllerStuff.Button.Start:
			return "START";
		case ControllerStuff.Button.X:
			return "X";
		case ControllerStuff.Button.Y:
			return "Y";
		default:
			return "OOPS";
		}
	}
}
