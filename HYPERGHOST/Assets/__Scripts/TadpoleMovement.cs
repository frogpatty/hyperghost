﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

[RequireComponent(typeof(Rigidbody2D))]
public class TadpoleMovement : MonoBehaviour {
	public int playerNum;

	private Rigidbody2D tadpoleRigidbody2D;
	private Animator animator;
	static float wiggleForce = 5.0f;
	private Vector2 moveDirection = Vector2.right;
	private float baseZRotation = 0f;
	LilyManager lilyManager;

	void Start () {
		lilyManager = FindObjectOfType<LilyManager>();
		if (playerNum == 0) playerNum = 1;
		tadpoleRigidbody2D = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator> ();
	}

	void Update () {
		animator.SetFloat ("currSpeed", tadpoleRigidbody2D.velocity.magnitude);

		ControllerStuff.Button downButton;
		ControllerStuff.Button upButton;

		if (GameManager.s.debugMode) {
			downButton = ControllerStuff.Button.DPadDown;
			upButton = ControllerStuff.Button.DPadUp;

		} else {
			downButton = LilyManager.s.tadpole_moveDown;
			upButton = LilyManager.s.tadpole_moveUp;
		}

		if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, downButton) || Input.GetKeyDown (KeyCode.H)) {
			WiggleDown();
		}
		else if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, upButton) || Input.GetKeyUp (KeyCode.Y)) {
			WiggleUp();
		} else {
			foreach (ControllerStuff.Button badButton in lilyManager.currbadButtons) {
				if (ControllerStuff.s.getButtonDown((PlayerIndex)playerNum, badButton)) {
					lilyManager.badButtonPressed(playerNum);
				}
			}
		}
	}
	
	void WiggleUp()
	{
		Debug.Log ("wiggling upRight");
		tadpoleRigidbody2D.AddForce((Vector2.up + moveDirection) * wiggleForce, ForceMode2D.Impulse);
		transform.rotation = Quaternion.Euler (0f, 0f, -10f + baseZRotation);
	}
	
	void WiggleDown()
	{
		Debug.Log ("wiggling Down left");
		tadpoleRigidbody2D.AddForce((Vector2.down + moveDirection) * wiggleForce, ForceMode2D.Impulse);
		transform.rotation = Quaternion.Euler (0f, 0f, -50f + baseZRotation);
	}

	void OnCollisionEnter2D(Collision2D collision2D) {
		ChangeDirection(collision2D);
	}

	void ChangeDirection(Collision2D collision2D) {
		if (moveDirection == Vector2.right) {
			moveDirection = Vector2.left;
			baseZRotation = 180f;
			transform.rotation = Quaternion.Euler (0f, 0f, baseZRotation);
		} else if (moveDirection == Vector2.left) {
			moveDirection = Vector2.right;
			baseZRotation = 0f;
			transform.rotation = Quaternion.Euler (0f, 0f, baseZRotation);
		}

		if (collision2D.transform.position.x > transform.position.x) {
			WiggleDown();
		} else if (collision2D.transform.position.x < transform.position.x) {
			WiggleUp();
		}
	}
}
