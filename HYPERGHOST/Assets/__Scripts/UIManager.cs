﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {
	private static UIManager m_myPrivateSelf;
	public static UIManager s {
		get {
			if (m_myPrivateSelf == null) m_myPrivateSelf = Object.FindObjectOfType<UIManager>();
			return m_myPrivateSelf;
		}
	}

	public GameObject titleScreen;
	public GameObject credits;

	void Awake() {
		titleScreen.SetActive (false);
		credits.SetActive (false);
		failscreen.SetActive (false);

		turnOnTitleScreen ();
	}

	public void turnOnTitleScreen() {
		titleScreen.SetActive (true);
	}

	public void turnOffTitleScreen() {
		iTween.ScaleTo (titleScreen, new Vector3(1,0,1), 0.5f);
	}

	public void turnOnCredits() {
		credits.SetActive (true);
	}

	public void turnOffCredits() {
		credits.SetActive (false);
	}

	public GameObject failscreen;
	public GameObject lilyObj;
	public void turnOnFailscreen() {
		failscreen.SetActive (true);
	}

	public void turnOffFailscreen() {
		failscreen.SetActive (false);
	}

	void Update() {
		if (failscreen.activeInHierarchy) {
			lilyObj.transform.RotateAround(lilyObj.transform.position, Vector3.forward, 360 * Time.deltaTime);
		}
	}
}
