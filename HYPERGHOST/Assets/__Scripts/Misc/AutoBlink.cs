﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AutoBlink : MonoBehaviour {
	static float bloop = 0.5f;

	void Start () {
		turnon ();
	}

	void turnon() {
		foreach (Graphic g in GetComponentsInChildren<Graphic>()) {
			g.enabled = true;
		}
		Invoke ("turnoff", bloop);
	}

	void turnoff() {
		foreach (Graphic g in GetComponentsInChildren<Graphic>()) {
			g.enabled = false;
		}
		Invoke ("turnon", bloop);
	}
}
