﻿using UnityEngine;
using System.Collections;

public class AutoSwerve : MonoBehaviour {
	static float factor = 10f;

	Vector3 originalPos;

	void Start() {
		originalPos = transform.position;
	}

	void Update () {
		Vector3 newPos = new Vector3 (originalPos.x + Mathf.Sin (Time.time * 5f) * factor, originalPos.y + Mathf.Sin (Time.time * 3f));
		transform.position = newPos;
	}
}
