﻿using UnityEngine;
using System.Collections;

public class Frogfood : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Tadpole" || other.tag == "Frog") {
			Debug.Log ("i hit a tadpole");
			getEaten();
		}
	}

	void getEaten() {
		GameManager.s.foodHasBeenEaten (this);
		iTween.ScaleTo (gameObject, Vector3.zero, Lily.lilyPopTime);
		Destroy (gameObject, Lily.lilyPopTime);
	}
}
