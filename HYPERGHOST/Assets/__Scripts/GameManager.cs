﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	private static GameManager m_myPrivateSelf;
	public static GameManager s {
		get {
			if (m_myPrivateSelf == null) m_myPrivateSelf = Object.FindObjectOfType<GameManager>();
			return m_myPrivateSelf;
		}
	}

	public enum Phase {
		NotStarted,
		Egg,
		Tadpole,
		Frog
	}

	public bool debugMode;

	public int numPlayers;
	public List<Color> playerColors;
	public List<Transform> eggSpawnPoints;
	public List<Transform> tadpole_foodGroups;
	public List<Transform> frog_foodGroups;

	public GameObject prf_egg;
	public GameObject prf_tadpole;
	public GameObject prf_frog;

	public Phase currPhase;
	public List<EggMovement> currEggs;
	public List<TadpoleMovement> currTadpoles;
	public List<Frogfood> currFrogfood;
	public List<FrogMovement> currFrogs;

	private bool gameStarted = false;
	private bool eggWigglingCommenced = false;

	void Awake() {
		foreach (Transform t in tadpole_foodGroups) t.gameObject.SetActive(false);
		foreach (Transform t in frog_foodGroups) t.gameObject.SetActive(false);
	}

	void Start() {
		currPhase = Phase.NotStarted;
		currEggs = new List<EggMovement> ();
		currTadpoles = new List<TadpoleMovement> ();
		currFrogfood = new List<Frogfood> ();
		currFrogs = new List<FrogMovement> ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Tab) || ControllerStuff.s.getButtonDown(XInputDotNetPure.PlayerIndex.One, ControllerStuff.Button.Start)) {
			if (!gameStarted) {
				UIManager.s.turnOffTitleScreen();
				enterPhase(Phase.Egg);
			} else if (!eggWigglingCommenced) {
				commenceEggWiggling();
			}

			if (theGameIsWon) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}

		if (Input.GetKeyDown(KeyCode.R)) {
			Reset();
		}
	}

	#region EGGS
	void beginGame() {;
		gameStarted = true;
		for (int i=0; i<numPlayers; i++) {
			Transform spawnPos;
			spawnPos = eggSpawnPoints[i];
			GameObject go = Instantiate (prf_egg, spawnPos.position, spawnPos.rotation) as GameObject;
			EggMovement e = go.GetComponentInChildren<EggMovement>();
			e.playerNum = i+1;
			e.OnBecomeTadpole += checkIfAllEggsEvolved; // listen for egg finished event
			currEggs.Add (e);
			go.GetComponentInChildren<SpriteRenderer>().color = playerColors[i];
		}
	}

	void commenceEggWiggling() {
		eggWigglingCommenced = true;
		foreach (EggMovement e in currEggs) {
			e.enableWiggling();
		}
	}

	void checkIfAllEggsEvolved() {
		foreach (EggMovement e in currEggs) {
			if (!e.isTadpole) return;
		}
		enterPhase (Phase.Tadpole);
	}
	#endregion

	#region TADPOLES
	void becomeTadpoles() {
		// make tadpoles
		foreach (EggMovement e in currEggs) {
			GameObject tad = Instantiate (prf_tadpole, e.transform.position, prf_tadpole.transform.rotation) as GameObject;
			TadpoleMovement t = tad.GetComponentInChildren<TadpoleMovement>();
			t.playerNum = e.playerNum;
			currTadpoles.Add (t);
			Destroy (e.gameObject);
		}
		currEggs.Clear ();

		// populate level with food
		for (int i=0; i<numPlayers; i++) {
			tadpole_foodGroups[i].gameObject.SetActive(true);
			foreach (Frogfood f in tadpole_foodGroups[i].GetComponentsInChildren<Frogfood>()) {
				currFrogfood.Add (f);
			}
		}
	}

	public void foodHasBeenEaten(Frogfood eatenFood) {
		currFrogfood.Remove (eatenFood);
		if (currFrogfood.Count <= 0) {
			if (currPhase == Phase.Tadpole) {
				enterPhase(Phase.Frog);
			} else if (currPhase == Phase.Frog) {
				Debug.Log ("the game is over!!! hurray!!!");
				winScreen();
			}
		}
	}
	#endregion

	#region FROGS
	void becomeFrogs() {
		// make frogs
		foreach (TadpoleMovement t in currTadpoles) {
			GameObject frog = Instantiate(prf_frog, t.transform.position, prf_frog.transform.rotation) as GameObject;
			FrogMovement f = frog.GetComponentInChildren<FrogMovement>();
			f.playerNum = t.playerNum;
			currFrogs.Add (f);
			Destroy (t.gameObject);
		}
		currTadpoles.Clear ();

		// populate level with food
		currFrogfood.Clear (); // make sure it's empty!!
		for (int i=0; i<numPlayers; i++) {
			frog_foodGroups[i].gameObject.SetActive(true);
			foreach (Frogfood f in frog_foodGroups[i].GetComponentsInChildren<Frogfood>()) {
				currFrogfood.Add (f);
			}
		}
	}

	private bool theGameIsWon = false;

	void winScreen() {
		UIManager.s.turnOnCredits ();
		theGameIsWon = true;
	}
	#endregion

	public void Reset() {
		UIManager.s.turnOnFailscreen ();
		Invoke ("doReset", 1.5f);
	}

	void doReset() {
		Application.LoadLevel(Application.loadedLevel);
	}

	public void enterPhase(Phase phase) {
		currPhase = phase;
		LilyManager.s.spawnLilies (phase);
		switch (phase) {
		case Phase.Egg:
			beginGame();
			break;
		case Phase.Tadpole:
			becomeTadpoles();
			break;
		case Phase.Frog:
			becomeFrogs();
			break;
		}
	}
}
