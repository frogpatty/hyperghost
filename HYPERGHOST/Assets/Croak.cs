﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Croak : MonoBehaviour {

	float howOftenToCroak = 0.6f;
	
	float lastTimeCroaked = 0.0f;

	public AudioClip ribbit;
	
	private AudioSource audioSource;

	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

		//croak
		if (Time.time - lastTimeCroaked > howOftenToCroak) {
			if (ControllerStuff.s.getButtonDown(PlayerIndex.One, ControllerStuff.Button.A) || Input.GetKeyDown (KeyCode.A)) {
				print ("CROAK");
				animator.SetBool("bCroaking", true);
				this.audioSource.PlayOneShot(ribbit);
				lastTimeCroaked = Time.time;
			}
		}
		else if (animator.GetBool("bCroaking")) {
			animator.SetBool("bCroaking", false);
		}
	}


}
